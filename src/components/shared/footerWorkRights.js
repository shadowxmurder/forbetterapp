import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {routeNames} from '../configs/constants/route-names';
import {Image, View, StyleSheet, Text, TouchableOpacity} from 'react-native';
/*
import {
    fontValue,
    heightPercentageToDP,
    widthPercentageToDP,
} from '../../../config/helpers/Dimensions';*/
function FooterWorkRights() {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      style={styles.background}
      onPress={() => {
        navigation.navigate(routeNames.login);
      }}>
      <View style={styles.background}>
        <View style={styles.logoContainer}>
          <View style={{margin: 16}}>
            <Image source={require('../assets/logo.png')} style={styles.logo} />
          </View>
          <Image source={require('../assets/For_better.png')} />
        </View>
        <Text>The Oracle of Your Rights!</Text>
      </View>
    </TouchableOpacity>
  );
}
const styles = StyleSheet.create({
  logo: {
    width: 64,
    height: 70,
  },
  logoContainer: {
    alignItems: 'center',
    padding: 16,
  },
  background: {
    flex: 1,
    backgroundColor: '#fffff',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
export default FooterWorkRights;
