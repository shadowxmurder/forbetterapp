import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, Image, TextInput, Alert } from 'react-native';
import ButtonPrincipal from '../configs/utils/buttonPrincipal'
import ButtonSecondary from '../configs/utils/buttonSecondary';
import { Dimensions } from 'react-native';
const window = Dimensions.get('window');
//import { axiosInstance, auth } from '../interceptors/tokenInterceptor';
import AsyncStorage from '@react-native-community/async-storage';
//import { hasInternetAccess } from '../shared/utility';
const screen = Dimensions.get('screen');
const Login = ({ navigation }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  useEffect(() => {
    _loadInitialState() // returns promise, so process in chain
      .then(value => {
        if (value !== null) {
          navigation.push('success')
        }
      });
  }, [])
  const _loadInitialState = async () => {
    const value = await AsyncStorage.getItem('token');
    return value;
  };
  const login = async () => {

    try {
      checkIfInternetAvailable();
      const res = await auth.login(email, password)
      await AsyncStorage.setItem('token', res.data.token);
      navigation.push('success')
    }
    catch (error) {
      alert(error)
    }
  };

  const checkIfInternetAvailable = async () => {
    const isInternetAvailable = await hasInternetAccess();
    if (!isInternetAvailable) {
      Alert.alert("You are not connected to the internet!", '', [{ text: 'OK', onPress: () => { } }], {
        cancelable: false
      });
    }
    return isInternetAvailable;
  };


  return (
    <View style={styles.background}>
      <View style={{ width: window.width }}>
        <Image
          style={{ width: window.width }}
          source={require('../assets/welcome.png')}
        />
        <View style={styles.logoCercle}>
          <Image
            style={{ alignContent: 'center' }}
            source={require('../assets/logo.png')}
          />
        </View>
      </View>
      <View style={styles.welcomeTextWrapper}>
        <View style={{ marginBottom: 16 }}>
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: 50,
              color: 'black',
              marginBottom: 5,
            }}>
            Log in.
          </Text>
        </View>
        <Text numberOfLines={4} style={{ color: '#696969', fontSize: 15 }}>
          Log in with your data that you entered during your
        </Text>
        <Text numberOfLines={2} style={{ color: '#696969', fontSize: 15 }}>
          registration.
        </Text>
      </View>

      <View style={styles.buttonWrapper}>
        <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'black' }}>
          E-mail
        </Text>
        <View style={styles.forms}>
          <TextInput
            style={styles.input}
            value={email}
            placeholder="insert your e-mail here"
            onChangeText={setEmail}
          />
        </View>

        <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'black' }}>
          Password
        </Text>
        <View style={styles.forms}>
          <TextInput
            style={styles.input}
            placeholder="insert your password here"
            value={password}
            onChangeText={setPassword}
            secureTextEntry={true}
          />
        </View>
      </View>
      <View style={{ alignItems: 'center' }}>
        <ButtonPrincipal title="success" onPress={login} >
          LOG IN
        </ButtonPrincipal>
        <Text style={{ fontSize: 14, color: 'black' }}>
          Don't have an account ?
        </Text>
        <ButtonSecondary>SIGN UP</ButtonSecondary>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  logoCercle: {
    padding: 10,
    backgroundColor: 'white',
    borderRadius: Math.round(window.width + window.height) / 2,
    width: 90,
    height: 90,
    top: -20,
    left: 40,
  },

  input: {
    borderRadius: 2,
    paddingTop: 10,
    borderBottomWidth: 1,
    borderColor: '#D4D4D4',
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    fontSize: 17,
    width: window.width / 1.2,
    backgroundColor: '#fff',
    color: '#424242',
  },
  welcomeTextWrapper: {
    flex: 1,
    alignItems: 'flex-start',
    paddingLeft: 16,
    paddingRight: 16,
  },
  forms: {},
  buttonWrapper: {
    top: -50,
    padding: 19,
  },
  background: {
    flex: 1,
    width: window.width,
    backgroundColor: '#FFFF',
    justifyContent: 'center',
  },
});
export default Login;
