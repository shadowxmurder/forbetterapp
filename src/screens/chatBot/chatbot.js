import React from 'react';
import {Image, View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import StarConversationButton from '../../configs/utils/starConversationButton';
import RoomPopoup from '../../configs/utils/roomPopup';
import { BlurView } from "@react-native-community/blur";

function Chatbot() {
  const [modalVisible, setModalVisible] = React.useState(true);
  return (
    <View style={styles.background} >
     
      <View style={styles.logoContainer}>
        <Image
          source={require('../../assets/brightenedLogo.png')}
          style={styles.logo}
        />
        <Text style={styles.noConversationText}>
          There is no conversations yet.
        </Text>
        <TouchableOpacity Onclick={() => setModalVisible(true)}>
          <StarConversationButton />
        </TouchableOpacity>
      </View>
      <RoomPopoup
        setModalVisible={setModalVisible}
        modalVisible={modalVisible}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  logo: {
    top: -150,
    width: 50,
    height: 54,
  },
  logoContainer: {
    alignItems: 'center',
    padding: 50,
  },
  background: {
    flex: 1,
    backgroundColor: '#fffff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  noConversationText: {
    fontSize: 30,
    textAlign: 'center',
    fontWeight: 'normal',
    top: -70,
    color: '#5B6A7E',
    fontStyle: 'normal',
    opacity: 0.9,
  },
});
export default Chatbot;
