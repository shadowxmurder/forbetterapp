import { Colors } from '../constants/colors';

const textShadow = {
  textShadowOffset: { height: 2, width: 0 },
  textShadowRadius: 2,
  textShadowColor: Colors.darkgray,
};

const viewShadow = (color?) => {
  return {
    shadowColor: color || Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.5,
    shadowRadius: 6.27,
    elevation: 10,
  };
};
export { textShadow, viewShadow };
