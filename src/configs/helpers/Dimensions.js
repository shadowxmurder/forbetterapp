/*
 * Realised by Mohamed Derbali
 * https://github.com/mohamedderbali
 * @format
 */

import {Dimensions, PixelRatio, Platform, StatusBar} from 'react-native';
export const screenHeight =
  Platform.OS === 'android'
    ? Dimensions.get('window').height + (StatusBar?.currentHeight || 0)
    : Dimensions.get('window').height;
export const screenWidth = Dimensions.get('window').width;

export const fontValue = (
  fontSize,
  standardScreenHeight = 812,
) => {
  const heightPercent = (fontSize * screenHeight) / standardScreenHeight;
  return PixelRatio.roundToNearestPixel(heightPercent);
};
export const widthPercentageToDP = (widthPercent) => {
  // Convert string input to decimal number
  return PixelRatio.roundToNearestPixel((screenWidth * widthPercent) / 100);
};

export const heightPercentageToDP = (heightPercent) => {
  // Convert string input to decimal number
  return PixelRatio.roundToNearestPixel((screenHeight * heightPercent) / 100);
};

export const vw = screenWidth / 100;
export const vh = screenHeight / 100;
