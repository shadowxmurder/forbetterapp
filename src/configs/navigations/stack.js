import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {routeNames} from '../constants/route-names';
//import { Login } from '../../ screens / auth / login';
//import { Signup } from '../../screens/auth/signup';
import Home from '../../screens/home';
import Login from '../../screens/login';
import Chatbot from '../../screens/chatBot/chatbot'
const Stack = () => {
  const StackNavigator = createNativeStackNavigator();
  return (
    <NavigationContainer>
      <StackNavigator.Navigator
        initialRouteName={routeNames.home}
        screenOptions={{headerShown: false, animation: 'slide_from_right'}}>
        <StackNavigator.Screen name={routeNames.home} component={Home} />
        <StackNavigator.Screen name={routeNames.login} component={Login} />
        <StackNavigator.Screen name={routeNames.chatbot} component={Chatbot} />
      </StackNavigator.Navigator>
    </NavigationContainer>
  );
};
export default Stack;
