import React, { useState } from 'react'
import { View, StyleSheet, Text, Image, Dimensions } from 'react-native'
import Video from 'react-native-video';

const { width, height } = Dimensions.get('window')
const EventCard = (props) => {
    const { video } = props
    console.log(video)
    return(
        (video === undefined) ? (
        <View style={styles.cardView}>
        <View style={styles.carouselCard}>
        <Image style={styles.image} source={require('../../assets/beyourself.png')} style={styles.imageContainer} />
        <View style={styles.logoCercle} >
        <View style={styles.logoCercle1} >
        <Text style={{color:'#FF703D',fontWeight:'bold',fontSize: 12}}>02</Text>
        <Text style={{color:'#FF703D',fontWeight:'bold',fontSize: 12}}>Nov</Text>
        </View>
        </View>
        <Text style={styles.itemTitle}>event test1</Text>
        <Text style={styles.itemDescription}>test description event 1</Text>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <Image style={{marginLeft:30,marginTop:10,marginBottom:5}} source={require('../../assets/heart.png')}  />
        <Text style={styles.texte}>Liked</Text>
        <Text style={styles.texte1}>#Arts</Text></View>
        
       
        </View>
        <View>
        
        </View>
    </View>
        ):(
        <View style={styles.cardView}>
        <View style={styles.carouselCard}>
        <Video
        source={{ uri: 'https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4?_=1' }}
        style={{ width: 300, height: 300 }}
        controls={true}
        style={styles.imageContainer}
        onBuffer={this.videoBuffer}
        ref={(ref) => {
        this.player = ref
        }} />
        
        <View style={styles.logoCercle} >
        <View style={styles.logoCercle1} >
        <Text style={{color:'#FF703D',fontWeight:'bold',fontSize: 12}}>02</Text>
        <Text style={{color:'#FF703D',fontWeight:'bold',fontSize: 12}}>Nov</Text>
        </View>
        </View>
        <Text style={styles.itemTitle}>event test1</Text>
        <Text style={styles.itemDescription}>test description event 1</Text>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <Image style={{marginLeft:30,marginTop:10,marginBottom:5}} source={require('../../assets/heart.png')}  />
        <Text style={styles.texte}>Liked</Text>
        <Text style={styles.texte1}>#Arts</Text></View>
        
       
        </View>
        <View>
        
        </View>
    </View>    
        )    
    )
}
const styles = StyleSheet.create({
    cardView: {
        flex: 1,
        width: width - 20,
        height: height / 3,
        backgroundColor: 'white',
        margin: 10,
        borderRadius: 20,
        shadowColor: '#000',
        shadowOffset: { width: 0.5, height: 0.5 },
        shadowOpacity: 0.5,
        shadowRadius: 3,
        elevation: 5,
    },
    texte: {
        color: '#FF703D',
        fontSize: 18,
        marginTop:6
    },
    texte1: {
        color: '#FF703D',
        fontSize: 18,
        marginTop:6,
        marginLeft:165,
        marginRight:30
    },
    textView: {
        backgroundColor: 'white',
        position: 'absolute',
        bottom: 10,
        margin: 10,
        left: 5,
    },
    image: {
        width: width - 20,
        height: height,
        borderRadius:20,
    },
    logoCercle: {
        width: 100,
        height: 100,
        borderRadius: 100/2,
        backgroundColor: 'white',
        alignItems: 'center',
        right:0,
        marginRight:20,
        marginTop:100,
        justifyContent: 'center',
        position:'absolute',
    },
    logoCercle1: {
        width: 80,
        height: 80,
        borderRadius: 100/2,
        backgroundColor: '#F5F6FA',
        alignItems: 'center',
        justifyContent: 'center',
        position:'absolute'
    },
    itemTitle: {
        marginLeft:30,
        marginTop:10,
        fontSize: 22,
        shadowColor: '#000',
        shadowOffset: { width: 0.8, height: 0.8 },
        shadowOpacity: 1,
        shadowRadius: 3,
        marginBottom: 5,
        fontWeight: "bold",
        elevation: 5
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
      },
    itemDescription: {
        marginLeft:30,
        fontSize: 12,
        shadowColor: '#000',
        shadowOffset: { width: 0.8, height: 0.8 },
        shadowOpacity: 1,
        shadowRadius: 3,
        elevation: 5
    },
    imageContainer:{
        height:'55%',
        width:'100%',
        borderRadius:20,
    },
    carouselCard:{
        flex:1,
        borderRadius:20,
    }
})
export default EventCard