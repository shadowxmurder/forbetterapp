import React from 'react';
import { TouchableOpacity, View, Text, StyleSheet, Dimensions} from 'react-native';

const deviceWidth= Dimensions.get("window").width 

const ButtonContinue = props =>(
    <TouchableOpacity onPress={props.onPress}>
        <View style={styles.button}>
            <Text style={styles.buttonText}>{props.children}</Text>
        </View>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    button:{
        borderRadius:16,
        width:deviceWidth/2.5,
        backgroundColor:'#FF5A20',
        padding:10,
        alignItems:'center',
        margin:8,
    },
    buttonText:{
        color:'white',
        fontWeight:'200',
    },
});
export default ButtonContinue;