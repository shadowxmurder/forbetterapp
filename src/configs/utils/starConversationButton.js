import React from 'react';
import {Image} from 'react-native';

function StarConversationButton() {
  return (
    <Image source={require('../../assets/Button.png')} style={{opacity: 0.9}} />
  );
}
export default StarConversationButton;
