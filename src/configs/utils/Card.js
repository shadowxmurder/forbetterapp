import React from 'react';
import {StyleSheet, View} from 'react-native';
import { Dimensions } from 'react-native';
const window = Dimensions.get("window");
export default function Card(props){
    return(
        <View style={styles.card}>
            <View style={styles.cardContent}>
                {props.children}

            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    card: {
        borderRadius:20,
        top:40,
        bottom:30,
        elevation:3,
        backgroundColor:'#fff',
        shadowOffset:{width:1, height:1},
        shadowColor:'#333',
        shadowOpacity: 0.3,
        shadowRadius:2,
        marginVertical:6,
        width:window.width/2.5,
        height:window.height/4,
        justifyContent:'center',
        alignItems:'center',
        marginHorizontal:4
    },
    cardContent:{
        marginHorizontal:18,
        marginVertical:10
    }
})