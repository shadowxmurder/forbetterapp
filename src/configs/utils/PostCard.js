import React, { useState } from 'react'
import { View, StyleSheet, Text, Image, Dimensions, TextInput } from 'react-native'

const { width, height } = Dimensions.get('window')
const PostCard = () => {
    const [ comment, setComment ]= useState('')
    return(
        <View style={styles.cardView}>
            
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <Image
        style={{alignContent: 'flex-end', alignSelf:'flex-end',  justifyContent:'flex-end'}}
        source={require('../../assets/postProfile.png')}
        />
        <View>
        <Text style={styles.texte}>Leslie Sheffield</Text>
        <Text style={styles.timetexte}>10 min</Text>
        </View>
        <Image
        style={{alignContent: 'flex-end', marginTop:5,justifyContent:'flex-end'}}
        source={require('../../assets/pts.png')}
        />
       
        </View>
            <Text style={styles.itemDescription}>Impedit possimus asperiores et cumque ipsam nesci tenetur vel quia ? #Wondering <Text style={styles.texte1}>#Arts</Text></Text>
        <View style={styles.carouselCard}>
        <Image style={styles.image} source={require('../../assets/event.jpg')} style={styles.imageContainer} />
        <View style={styles.logoCercle} >
      
        </View>    

        </View>
        <View style={{ borderBottomColor: '#E8E8E8',borderBottomWidth: 1,top:50}}/>
        <View style={{flex: 1,justifyContent:'space-between', flexDirection:'row',top:50, padding:20}}>
            <View style={{ flexDirection:'row' }}>
            <Image
            style={{alignContent: 'center'}}
            source={require('../../assets/like.png')}
            />
            <Text style={{left:8,fontSize:15,fontFamily:'TT Norms Pro'}}>Like</Text>
           
            </View>
             <View style={styles.verticleLine}></View>
            <View style={{ flexDirection:'row' }}>
            <Image
            style={{alignContent: 'center'}}
            source={require('../../assets/comment.png')}
            />
            <Text style={{left:8,fontSize:15,fontFamily:'TT Norms Pro'}}>Comment</Text>
           
            </View>
            <View style={styles.verticleLine}></View>
            <View style={{ flexDirection:'row' }}>
            <Image
            style={{alignContent: 'center'}}
            source={require('../../assets/share.png')}
            />
            <Text style={{left:8,fontSize:15,fontFamily:'TT Norms Pro'}}>Share</Text>
           
            </View>
        </View>
        <View style={{ borderBottomColor: '#E8E8E8',borderBottomWidth: 1}}/>
        <View style={{justifyContent:'space-between', flexDirection:'row', padding:20}}>
           
            <View style={{ flexDirection:'row' }}>
            <Image
            style={{alignContent: 'center'}}
            source={require('../../assets/user.png')}
            />
            <TextInput
            style={styles.input}
            onChangeText={(e) => setComment(e)}
            placeholder="Write a comment ...."
            style={styles.mindinput}
            value={comment}
            />
            </View>
        
        </View>
    </View>
    )
}
const styles = StyleSheet.create({
    cardView: {
        flex: 1,
        width: width - 20,
        height: height -350 ,
        backgroundColor: 'white',
        margin: 10,
        borderRadius: 20,
        shadowColor: '#000',
        shadowOffset: { width: 0.5, height: 0.5 },
        shadowOpacity: 0.5,
        shadowRadius: 3,
        elevation: 5,
        padding:'5%'
    },
    input: {
        borderRadius:2,
        marginLeft:10,
        paddingTop: 10,
        borderBottomWidth:1,
        borderColor:'#D4D4D4',
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 0,
        fontSize:20,
        width:window.width/1.2,
        backgroundColor: '#F5F6FA',
        color: '#F5F6FA',
        padding:5,
    },
    texte: {
        marginLeft:-110,
        fontSize: 18,
        marginTop: 5
    },
    verticleLine: {
        height: '8%',
        width: 1,
        backgroundColor: '#E8E8E8',
    },
    timetexte: {
        marginLeft:-110,
        fontSize: 11
    },
    texte1: {
        color: '#FF703D',
        fontSize: 18,
       
    },
    textView: {
        backgroundColor: 'white',
        position: 'absolute',
        bottom: 10,
        margin: 10,
        left: 5,
    },
    image: {
        width: width - 20,
        height: height,
        borderRadius:20,
    },
    itemTitle: {
        marginLeft:30,
        marginTop:10,
        fontSize: 22,
        shadowColor: '#000',
        shadowOffset: { width: 0.8, height: 0.8 },
        shadowOpacity: 1,
        shadowRadius: 3,
        marginBottom: 5,
        fontWeight: "bold",
        elevation: 5
    },
    itemDescription: {
        marginTop:'2%',
        marginBottom:'7%',
        fontSize: 12,
        shadowColor: '#000',
        shadowOffset: { width: 0.8, height: 0.8 },
        shadowOpacity: 1,
        shadowRadius: 3,
        elevation: 5
    },
    imageContainer:{
        height:'100%',
        
        width:'100%',
        borderRadius:20,
        flex:1,
        alignContent:'center',
        alignSelf:'center',
        justifyContent:'center'
    },
    carouselCard:{
        flexDirection:'row',
        flex:1,
        borderRadius:20,
    }
})
export default PostCard